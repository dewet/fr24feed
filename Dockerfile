FROM busybox AS build
ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM" > /log

ADD https://repo-feed.flightradar24.com/rpi_binaries/fr24feed_1.0.25-3_armhf.tgz /tmp/
ADD https://repo-feed.flightradar24.com/linux_x86_64_binaries/fr24feed_1.0.24-5_amd64.tgz /tmp/
WORKDIR /tmp
RUN \
  mkdir -p linux/arm/v7 && \
  mkdir -p linux/arm64 && \
  mkdir -p linux/amd64 && \
  tar -xzf fr24feed_*_armhf.tgz fr24feed_armhf/fr24feed && \
  cp fr24feed_armhf/fr24feed linux/arm/v7/ && \
  cp fr24feed_armhf/fr24feed linux/arm64/ && \
  tar -xzf fr24feed_*_amd64.tgz fr24feed_amd64/fr24feed && \
  cp fr24feed_armhf/fr24feed linux/amd64/

FROM scratch
ARG TARGETPLATFORM
COPY --from=build /tmp/${TARGETPLATFORM}/fr24feed /fr24feed
ENTRYPOINT [ "/fr24feed" ]
